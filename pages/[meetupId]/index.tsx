import { Fragment } from "react";
import Head from "next/head";
import { MongoClient, ObjectId } from "mongodb";
import MeetupDetail from "../../components/meetups/MeetupDetail";

function MeetupDetails(props: any) {
  return (
    <>
      <Head>
        <title>{props.meetupData.title}</title>
        <meta name="description" content={props.meetupData.description} />
      </Head>
      <MeetupDetail
        image={props.meetupData.image}
        title={props.meetupData.title}
        address={props.meetupData.address}
        description={props.meetupData.description}
      />
    </>
  );
}

export async function getStaticPaths() {
  const client = await MongoClient.connect(
    "mongodb://root:example@localhost:27017"
  );
  const db = client.db("Meetups");
  const meetupsCollection = db.collection("meetups");

  const meetups = await meetupsCollection.find({}).toArray(); // find(_, fieldsToInclude), include only _id

  client.close();
  return {
    fallback: false,
    paths: meetups.map((meetup) => ({
      params: { meetupId: meetup._id.toString() },
    })),
  };
}

export async function getStaticProps(context: any) {
  // fetch data for a single meetup
  const { meetupId } = context.params;

  const client = await MongoClient.connect(
    "mongodb://root:example@localhost:27017"
  );
  const db = client.db("Meetups");
  const meetupsCollection = db.collection("meetups");

  const selectedMeetup = await meetupsCollection.findOne({
    _id: new ObjectId(meetupId),
  });

  client.close();

  return {
    props: {
      meetupData: {
        id: selectedMeetup!._id.toString(),
        title: selectedMeetup!.title,
        address: selectedMeetup!.address,
        image: selectedMeetup!.image,
        description: selectedMeetup!.description,
      },
    },
  };
}

export default MeetupDetails;
