import { MongoClient } from "mongodb";

// /api/new-meetup
// POST /api/new-meetup

// Connection URL
const url = "mongodb://root:example@localhost:27017";
const client = new MongoClient(url);
// Database Name
const dbName = "Meetups";

// Use connect method to connect to the server

// the following code examples can be pasted here...

// main()
// .then(console.log)
// .catch(console.error)
// .finally(() => client.close());

import { NextApiRequest, NextApiResponse } from "next";
import { Meetup } from "../../models/Meetup.model";

async function handler(req: NextApiRequest, res: NextApiResponse) {
  console.log("POST /api/new-meetup");
  if (req.method === "POST") {
    const data: Meetup = req.body;

    console.log("BEFORE CONNECT");
    await client.connect();

    console.log("\n\nConnected successfully to server\n\n");

    const db = client.db(dbName);
    const meetupsCollection = db.collection("meetups");

    const result = await meetupsCollection.insertOne(data);

    console.log("\nresult is", result);

    client.close();

    res.status(201).json({ message: "Meetup inserted" });
  }
}

export default handler;
