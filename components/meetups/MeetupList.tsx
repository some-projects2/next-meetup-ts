import { Meetup } from "../../models/Meetup.model";
import MeetupItem from "./MeetupItem";

import styles from "./MeetupList.module.css";

interface Props {
  meetups: Meetup[] | undefined;
}

function MeetupList(props: Props) {
  return (
    <ul className={styles.list}>
      {props.meetups?.map((m) => (
        <MeetupItem key={m.id} {...m} />
      ))}
    </ul>
  );
}

export default MeetupList;
