import { useRouter } from "next/router";

import Card from "../ui/Card";
import styles from "./MeetupItem.module.css";

interface Props {
  id: string;
  image: string;
  title: string;
  address: string;
  description: string;
}

function MeetupItem(props: Props) {
  const router = useRouter();

  function showDetailsHandler() {
    router.push("/" + props.id);
  }

  return (
    <Card>
      <li className={styles.item}>
        <div className={styles.image}>
          <img src={props.image} alt={props.title} />
        </div>
        <div className={styles.content}>
          <h3>{props.title}</h3>
          <address>{props.address}</address>
          <p>{props.description}</p>
        </div>
        <div className={styles.actions}>
          <button onClick={showDetailsHandler}>Show Details</button>
        </div>
      </li>
    </Card>
  );
}

export default MeetupItem;
