import React from "react";
import styles from "./Layout.module.css";
import MainNavigation from "./MainNavigation";

function Layout(props: { children: React.ReactNode }) {
  return (
    <div className={styles.layout}>
      <MainNavigation />
      <main className={styles.main}>{props.children}</main>
    </div>
  );
}

export default Layout;
